package three.kyu;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {
    public static Double evaluate(String expression) {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
        Double result = null;
        try {
            result = Double.valueOf(engine.eval(expression).toString());
        } catch (ScriptException e) {
            e.getMessage();
        }
        return result;
    }
}
