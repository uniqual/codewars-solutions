package five.kyu;

import java.util.Stack;

public class DirReduction {

    public static String[] dirReduc(String[] arr) {

        Stack<String> stack = new Stack<>();

        for (String direction : arr) {
            switch (direction) {
                case "NORTH":
                    if (!stack.isEmpty() && "SOUTH".equals(stack.lastElement())) stack.pop();
                    else stack.push(direction);
                    break;
                case "SOUTH":
                    if (!stack.isEmpty() && "NORTH".equals(stack.lastElement())) stack.pop();
                    else stack.push(direction);
                    break;
                case "WEST":
                    if (!stack.isEmpty() && "EAST".equals(stack.lastElement())) stack.pop();
                    else stack.push(direction);
                    break;
                case "EAST":
                    if (!stack.isEmpty() && "WEST".equals(stack.lastElement())) stack.pop();
                    else stack.push(direction);
                    break;
            }
        }
        return stack.toArray(new String[0]);
    }

}
