package four.kyu;

import java.util.ArrayList;
import java.util.List;

public class NextSmallerNumber {

    public static long nextSmaller(long n) {
        List<Long> list = new ArrayList<>();

        while (n > 0) {
            list.add(n % 10);
            n = n / 10;
        }

        return n;
    }
}
