package six.kyu;

import java.util.HashMap;
import java.util.Map;

// Duplicate Encoder

public class DuplicateEncoder {
    static String encode(String word) {

        char[] chars = word.toLowerCase().toCharArray();

        Map<Character, Character> map = new HashMap<>();
        for(char c : chars)
        {
            if(map.containsKey(c)) {
                map.put(c, ')');
            } else {
                map.put(c, '(');
            }
        }

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < chars.length; i++) {
            for (char ch : map.keySet()) {
                if (chars[i] == ch) {
                    stringBuilder.append(map.get(ch));
                }
            }
        }
        return stringBuilder.toString();
    }
}