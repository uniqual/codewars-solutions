package six.kyu;

public class DRoot {
    public static int digital_root(int n) {
        int sum;
        do {
            sum = 0;
            while (n > 0) {
                sum = sum + (n % 10);
                n = n / 10;
            }
            n = sum;
        } while (sum > 10);

        return sum;
    }
}
